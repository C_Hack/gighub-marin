﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GigHub_Marin_.Startup))]
namespace GigHub_Marin_
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
