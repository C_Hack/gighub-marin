﻿using GigHub_Marin_.Entities;
using GigHub_Marin_.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GigHub_Marin_.Context
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
        {
            public ApplicationContext()
                : base("GigHub", throwIfV1Schema: false)
            {
            }

            public static ApplicationContext Create()
            {
                return new ApplicationContext();
            }



        public  DbSet<Genre> Genres { get; set;}
        public DbSet<Gig> Gigs { get; set;}


    }


}
